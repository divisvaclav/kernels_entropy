import cv2
import numpy as np
import os
import json
import copy


def open_labels(path_to_labels):
    extension = path_to_labels.split(".")[-1]
    assert extension != "json" or extension != "txt", "Unknown format of labels."

    if extension == "json":
        with open(path_to_labels) as jsonFile:
            labels = json.load(jsonFile)
            jsonFile.close()
    else:
        with open(path_to_labels) as f:
            labels = list()
            for line in f:
                labels.append(line)

    return len(labels), labels


def set_key(dictionary, key, value):
    if key not in dictionary:
        dictionary[key] = [value]
    elif type(dictionary[key]) == list:
        dictionary[key].append(value)
    else:
        dictionary[key] = [dictionary[key], value]


def open_txt_as_dict(path):
    d = {}
    with open(path) as f:
        for line in f:
            (key, val) = line.split()
            d[int(key)] = val
    return d


def open_txt_as_list(path):
    l = []
    with open(path) as f:
        for line in f:
            (key, output_class, rest) = line.split("'", 2)
            l.append(output_class)
    return l


def open_txt_as_numpy(path):
    l = []
    with open(path) as f:
        for line in f:
            values = line.rstrip().split(",")
            l.append(np.array(values, dtype=np.float32))
    return l


def save_numpy_array(_np_data, _path, _name, _index=None):
    # add a 'best fit' line
    weights_path = os.path.join(_path, _name)

    if not os.path.exists(weights_path):
        os.makedirs(weights_path)
    if _index:
        np.save(os.path.join(weights_path, "%04d_" % (_index)) + _name, _np_data)
    else:
        np.save(weights_path, _np_data)


def xyxy2xywh(x):
    # Convert nx4 boxes from [x1, y1, x2, y2] to [x, y, w, h] where xy1=top-left, xy2=bottom-right
    y = np.empty(shape=x.shape)
    y[0] = x[0]  # - x[:, 2] / 2  # top left x
    y[1] = x[1]  # - x[:, 3] / 2  # top left y
    y[2] = x[2] - x[0]  # bottom right x
    y[3] = x[3] - x[1]  # bottom right y
    return y


def xywh2xyxy_1d(x):
    # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
    y = np.empty(shape=x.shape)
    y[0] = x[0]  # - x[:, 2] / 2  # top left x
    y[1] = x[1]  # - x[:, 3] / 2  # top left y
    y[2] = x[0] + x[2]  # bottom right x
    y[3] = x[1] + x[3]  # bottom right y
    return y

def xywh2topleft_bottomright(x):
    # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
    y = np.empty(shape=x.shape)
    y[0] = x[0]  # - x[:, 2] / 2  # top left x
    y[1] = x[1]  # - x[:, 3] / 2  # top left y
    y[2] = x[0] + x[2]  # bottom right x
    y[3] = x[1] + x[3]  # bottom right y
    return (int(y[0]), int(y[1])), (int(y[2]), int(y[3]))


def xywh2xyxy(x):
    # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
    y = np.empty(shape=x.shape)
    y[:, 0] = x[:, 0]  # - x[:, 2] / 2  # top left x
    y[:, 1] = x[:, 1]  # - x[:, 3] / 2  # top left y
    y[:, 2] = x[:, 0] + x[:, 2]  # bottom right x
    y[:, 3] = x[:, 1] + x[:, 3]  # bottom right y
    return y


def xyxy_abs_to_rel_bbox(boxes, im_width, im_height):
    boxes_xy = np.empty(shape=boxes.shape)
    boxes_xy[:, 0] = boxes[:, 0] / im_width
    boxes_xy[:, 1] = boxes[:, 1] / im_height
    boxes_xy[:, 2] = boxes[:, 2] / im_width
    boxes_xy[:, 3] = boxes[:, 3] / im_height
    return boxes_xy


