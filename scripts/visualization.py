import os
import copy
import collections
from tqdm import tqdm
import numpy as np
import cv2
import math
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter

plt.rcParams.update({'figure.max_open_warning': 0})

analyzed_labels = ["person", "car", "bicycle", "motorcycle", "bus", "train",
                   "truck", "traffic light", "street sign", "stop sign"]

remove_string = ["head.", "module_list.", ".weight", "backbone.body.", "backbone.", "roi_heads."]



@staticmethod
def setKey(dictionary, key, value):
    if key not in dictionary:
        dictionary[key] = [value]
    elif type(dictionary[key]) == list:
        dictionary[key].append(value)
    else:
        dictionary[key] = [dictionary[key], value]



    @staticmethod
    def plot_all(plot_path, name, x, y, label, exp_decay_f=None, legends="None", xlabel="None", ylabel="None",
                 title="None", figsize=(6, 4)):
        """

        """
        normalize = True
        plt.rcParams.update({'font.size': 10})
        from scipy.stats import wasserstein_distance

        if label is not None:
            save_path = os.path.join(plot_path, "visualization", label)
        else:
            save_path = os.path.join(plot_path, "visualization")

        if not os.path.exists(save_path):
            os.makedirs(save_path)

        fig, ax = plt.subplots(figsize=figsize)
        #fig, ax = plt.subplots()

        if not isinstance(x, np.ndarray):
            for _x, _y, legend in zip(x, y, legends):
                if len(_x) > 0:
                    num_samples = _x[2]
                    x_values = _y[1][:100]
                    y_values = (np.asarray(_x[0]))[:100]
                    y_values_norm = y_values / np.max(y_values)
                    uniform_dist = np.ones(100, dtype=float)
                    was_dist = wasserstein_distance(y_values_norm, uniform_dist)
                    if xlabel == "BB relative size [-]":
                        if normalize:
                            ax.bar(x_values, y_values_norm, 0.01,
                                    label=legend + " $n_{samp}=$" + "${}$".format(num_samples) + ", $w_{dist}=$" + "{:.2f}".format(was_dist), alpha=0.7)

                    elif xlabel == "Distance to object [m]":
                        # gather final statistics
                        ind_of = np.where(np.asarray(x_values) > lim_highway)[0]
                        sum_of_highway_obj = np.sum(y_values[ind_of[0]:])
                        print("{}, {}".format(name, legend))
                        print("Number of objects further than: {} is: {}".format(lim_highway, sum_of_highway_obj))
                        print("Wasserstein dist. is {}".format(was_dist))
                        if normalize:
                            ax.bar(x_values, y_values_norm,
                                    label=legend + " $n_{samp}=$" + "${}$".format(num_samples) + ", $w_{dist}=$" + "{:.2f}".format(was_dist), alpha=0.7)

                        y_max = ax.get_ylim()[1]
                        y_max_margin = y_max - y_max * 0.03
                        x_offset = 3.5
                        ax.plot([lim_highway, lim_highway], [0.0, y_max], "r")#, label='for 130km/h')
                        #ax.text(lim_highway - x_offset, y_max_margin, str(lim_highway),
                        #        bbox=dict(facecolor='white', edgecolor='white', boxstyle='round'))


        # ax.invert_yaxis()  # labels read top-to-bottom
        ax.set_ylabel(ylabel)
        ax.set_xlabel(xlabel)
        ax.set_title(title)
        ax.legend()

        fig.tight_layout()
        # plt.show()
        plt.savefig(os.path.join(save_path, name + ".pdf"), dpi=300)
        # plt.show()

    @staticmethod
    def plot_histogram_of_dataset(_np_data,
                                  _bar_path,
                                  _name,
                                  _title,
                                  _label,
                                  _x_label="None",
                                  _y_label="None",
                                  _legend="None",
                                  r_min=0.0, r_max=1.0,
                                  num_bins=100,
                                  _np_data_s=None):

        plt.rcParams.update({'font.size': 12})

        if len(_np_data.shape) > 0:
            conf_path = os.path.join(_bar_path, "visualization", _label)
            if not os.path.exists(conf_path):
                os.makedirs(conf_path)

            fig, ax = plt.subplots(figsize=(8, 4))
            mu = np.mean(_np_data)
            sigma = np.std(_np_data)
            if _np_data_s is not None:
                label_primary = "$^y: \mu={:.2f}$, $\sigma={:.2f}$".format(mu, sigma)
            else:
                label_primary = "$\mu={:.2f}$, $\sigma={:.2f}$".format(mu, sigma)

            n, bins, patches = ax.hist(_np_data, num_bins, range=(r_min, r_max), color='b', rwidth=0.8, alpha=0.7)  # range=(np.min(_np_data), np.max(_np_data))

            if _np_data_s is not None:
                n_p, bins_p, patches_p = ax.hist(_np_data_s, num_bins, range=(r_min, r_max),
                                                 color='r',
                                                 label="$\^y: \mu={:.2f}$, $\sigma={:.2f}$".format(np.mean(_np_data_s),
                                                                                                   np.std(_np_data_s)),
                                                 alpha=0.7,
                                                 rwidth=0.8)
                mAP_rel_size = (n_p / n) * 100.0
                fig_mAP, ax_mAP = plt.subplots(figsize=(8, 4))
                ind = np.arange(num_bins)
                ind_label = np.linspace(0, 100, num=11, dtype=int)
                labels = np.linspace(0.0, 1.0, num=11)
                ax_mAP.bar(ind, mAP_rel_size, label="mAP: {}".format(np.mean(mAP_rel_size)), alpha=0.7)
                ax_mAP.plot(ind, [np.mean(mAP_rel_size)] * 100, color="r", alpha=0.7)
                ax_mAP.set_ylabel('mAP')
                ax_mAP.set_xlabel('BB rel. size')
                ax_mAP.set_title('mAP over BB rel. size')
                # ax_mAP.set_xticks(ind, labels=labels.tolist())
                ax_mAP.grid()
                ax_mAP.legend()
                fig_mAP.tight_layout()
                fig_mAP.savefig(os.path.join(conf_path, _name + "_mAP_hist.png"), dpi=300)

            ax.set_ylabel(_y_label)
            ax.set_xlabel(_x_label)
            ax.set_title(_title)

            if _x_label == "Distance to object [m]" and _legend == "None":
                y_max = ax.get_ylim()[1]
                y_max_margin = y_max - y_max*0.03
                x_offset = 3.5
                # plot the speed limits and corresponding legend
                ax.plot([lim_highway, lim_highway], [0.0, y_max], "r", label='for 130km/h')
                ax.text(lim_highway - x_offset, y_max_margin, str(lim_highway),
                        bbox=dict(facecolor='white', edgecolor='white', boxstyle='round'))
                ax.plot([lim_motorway, lim_motorway], [0.0, y_max], color='orange', label='for 100km/h')
                ax.text(lim_motorway - x_offset, y_max_margin, str(lim_motorway),
                        bbox=dict(facecolor='white', edgecolor='white', boxstyle='round'))
                ax.plot([lim_city, lim_city], [0.0, y_max], "g", label='for 50km/h')
                ax.text(lim_city - x_offset, y_max_margin, str(lim_city),
                        bbox=dict(facecolor='white', edgecolor='white', boxstyle='round'))
                ax.legend(title="Minimum safe distance in [m]", framealpha=1.0)
            else:
                # plot just mean and std
                ax.legend(framealpha=1.0)

            plt.axvline(mu, color='k', linestyle='dashed', linewidth=1)
            min_ylim, max_ylim = plt.ylim()
            plt.text(mu * 1.1, max_ylim * 0.9, 'Mean: {:.2f}'.format(mu))

            fig.tight_layout()
            # plt.show()
            fig.savefig(os.path.join(conf_path, _name + "_hist.png"), dpi=300)
            fig.savefig(os.path.join(conf_path, _name + "_hist.pdf"), dpi=300)

            return n, bins, patches

    @staticmethod
    def canvas2rgb_array(canvas):
        """Adapted from: https://stackoverflow.com/a/21940031/959926"""
        canvas.draw()
        buf = np.frombuffer(canvas.tostring_rgb(), dtype=np.uint8)
        ncols, nrows = canvas.get_width_height()
        scale = round(math.sqrt(buf.size / 3 / nrows / ncols))
        return buf.reshape(scale * nrows, scale * ncols, 3)


    # Inverse of the preprocessing and plot the image
    @staticmethod
    def plot_img(x):
        """
        x is a BGR image with shape (? ,224, 224, 3)
        """
        t = np.zeros_like(x[0])
        t[:, :, 0] = x[0][:, :, 2]
        t[:, :, 1] = x[0][:, :, 1]
        t[:, :, 2] = x[0][:, :, 0]
        # plt.imshow(np.clip((t+[123.68, 116.779, 103.939]), 0, 1.0))
        # plt.imshow(np.clip(t, 0, 1.0))
        # plt.imshow(t)
        plt.grid('off')
        plt.axis('off')
        # plt.show()

    @staticmethod
    def plot_x_y(plot_path, name, x, y, label, exp_decay_f=None, legends="None", xlabel="None", ylabel="None",
                 title="None", figsize=(8, 4)):
        """

        """
        normalize = True
        plt.rcParams.update({'font.size': 10})

        if label is not None:
            save_path = os.path.join(plot_path, "visualization", label)
        else:
            save_path = os.path.join(plot_path, "visualization")

        if not os.path.exists(save_path):
            os.makedirs(save_path)

        fig, ax = plt.subplots(figsize=figsize)
        #fig, ax = plt.subplots()

        if not isinstance(x, np.ndarray):
            for _x, _y, legend in zip(x, y, legends):
                if len(_x) > 0:
                    num_samples = _x[2]
                    if normalize:
                        ax.plot(_y[1][:_x[0].shape[0]], _x[0] / np.max(_x[0]),
                                label=legend + " [" + str(num_samples) + "]", alpha=0.7)
                    else:
                        ax.plot(_y[1][:_x[0].shape[0]], _x[0],
                                label=legend + " [" + str(num_samples) + "]", alpha=0.7)
        else:
            # mark red the outliers
            colors = ["blue"] * len(x)
            if exp_decay_f is not None:
                yi = exp_decay_f(x)

                for index, (y_gt, y_calc) in enumerate(zip(y, yi)):
                    if y_calc < y_gt:
                        colors[index] = "red"
            # scale = np.asarray((x+0.1)*5, dtype=float)
            if legends == "None":
                ax.scatter(x, y, c=colors, alpha=0.7)  # , s=scale) # label=legends
            else:
                ax.scatter(x, y, c=colors, label=legends, alpha=0.7)

        # ax.invert_yaxis()  # labels read top-to-bottom
        ax.set_ylabel(ylabel)
        ax.set_xlabel(xlabel)
        ax.set_title(title)
        ax.legend()

        if xlabel == "Distance to object [m]" and legends == "None":
            # hardcoded the limits of y axis to be 1.0
            lin_space = np.linspace(0.0, 120.0, 120)
            exp_decay = exp_decay_f(lin_space)
            # $e^{-\frac{x}{6.0} + 0.075}$
            ax.plot(lin_space, exp_decay, "black", label=r'$e^{-\frac{x}{6.0} + 0.075}$', alpha=0.7)

            y_max = ax.get_ylim()[1]
            y_max_margin = y_max - y_max * 0.03
            x_offset = 3.5
            ax.plot([lim_highway, lim_highway], [0.0, y_max], "r", label='for 130km/h')
            ax.text(lim_highway - x_offset, y_max_margin, str(lim_highway),
                    bbox=dict(facecolor='white', edgecolor='white', boxstyle='round'))
            ax.plot([lim_motorway, lim_motorway], [0.0, y_max], color='orange', label='for 100km/h')
            ax.text(lim_motorway - x_offset, y_max_margin, str(lim_motorway),
                    bbox=dict(facecolor='white', edgecolor='white', boxstyle='round'))
            ax.plot([lim_city, lim_city], [0.0, y_max], "g", label='for 50km/h')
            ax.text(lim_city - x_offset, y_max_margin, str(lim_city),
                    bbox=dict(facecolor='white', edgecolor='white', boxstyle='round'))
            ax.legend(title="Minimum safe distance in [m]", framealpha=1.0)

        fig.tight_layout()
        # plt.show()
        plt.savefig(os.path.join(save_path, name + ".png"), dpi=300)
        plt.savefig(os.path.join(save_path, name + ".pdf"), dpi=300)
        # plt.show()

    @staticmethod
    def plot_histogram_of_confidence(_np_data, _bar_path, _name, _labels, _title, _index="_"):

        conf_path = os.path.join(_bar_path, _name)
        if not os.path.exists(conf_path):
            os.makedirs(conf_path)

        x = np.arange(len(_labels))
        width = 0.8

        fig, ax = plt.subplots()
        bar_ref = ax.bar(x, _np_data, width, color='r')
        ax.set_ylabel('Confidences')
        ax.set_title(_title)
        ax.set_xticks(x)
        ax.set_xticklabels(_labels)
        ax.set_yticks(np.arange(0, 1.1, 0.1))
        ax.yaxis.set_major_formatter(FormatStrFormatter('%.4f'))

        # ax.set_xticklabels(_labels)
        # ax.legend()

        def autolabel(rects):
            """Attach a text label above each bar in *rects*, displaying its height."""
            for rect in rects:
                height = rect.get_height()
                ax.annotate('{:.4f}'.format(height),
                            xy=(rect.get_x() + rect.get_width() / 2, height),
                            xytext=(0, 3),  # 3 points vertical offset
                            textcoords="offset points",
                            ha='center', va='bottom')

        autolabel(bar_ref)
        fig.tight_layout()
        # plt.show()
        plt.savefig(os.path.join(conf_path, "%04d_histogram.png" % (_index)), dpi=300)

    @staticmethod
    def plot_histogram_of_weights(_np_data, _hist_path, _name, _index, _title):
        num_bins = 50
        fig, (ax_R, ax_G, ax_B) = plt.subplots(3, 1)
        # fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
        # the histogram of the data
        # weights_norm = np.ones_like(_np_data)/float(len(_np_data))
        n_R, bins_R, patches = ax_R.hist(_np_data[:, :, 0].flatten(), num_bins, density=1, color='r')
        n_G, bins_G, patches = ax_G.hist(_np_data[:, :, 1].flatten(), num_bins, density=1, color='g')
        n_B, bins_B, patches = ax_B.hist(_np_data[:, :, 2].flatten(), num_bins, density=1, color='b')
        # print(len(bins))
        # print(bins)
        # n, bins, patches = ax.hist(np_weights, num_bins, weights=weights_norm)
        # print(bins)
        mean_R = np.mean(_np_data[:, :, 0])
        std_R = np.std(_np_data[:, :, 0])
        mean_G = np.mean(_np_data[:, :, 1])
        std_G = np.std(_np_data[:, :, 1])
        mean_B = np.mean(_np_data[:, :, 2])
        std_B = np.std(_np_data[:, :, 2])

        # add a 'best fit' line
        hist_path = os.path.join(_hist_path, _name)
        if not os.path.exists(hist_path):
            os.makedirs(hist_path)

        # y_R = ((1 / (np.sqrt(2 * np.pi) * std_R)) * np.exp(-0.5 * (1 / std_R * (bins_R - mean_R))**2))
        # ax_R.plot(bins_R, y_R, '--')
        # axs[moment].tick_params(axis='both', which='major', labelsize=6)
        # axs[moment].tick_params(axis='both', which='minor', labelsize=4)
        # axs[moment].plot(bins, y, '--')
        # axs[moment].set_ylabel('Density', fontsize=6)
        # axs[moment].set_xlabel('Value', fontsize=6)
        # axs[moment].grid(True)
        ax_R.set_xlabel('Weight')
        ax_R.set_ylabel('Probability density')
        ax_R.set_title(r'{} with $\mu={:.4f}$, $\sigma={:.4f}$'.format(_title, mean_R, std_R))

        # y_G = ((1 / (np.sqrt(2 * np.pi) * std_G)) * np.exp(-0.5 * (1 / std_G * (bins_G - mean_G))**2))
        # ax_G.plot(bins_G, y_G, '--')
        ax_G.set_xlabel('Weight')
        ax_G.set_ylabel('Probability density')
        ax_G.set_title(r'{} with $\mu={:.4f}$, $\sigma={:.4f}$'.format(_title, mean_G, std_G))

        # y_B = ((1 / (np.sqrt(2 * np.pi) * std_B)) * np.exp(-0.5 * (1 / std_B * (bins_B - mean_B))**2))
        # ax_B.plot(bins_B, y_B, '--')
        ax_B.set_xlabel('Weight')
        ax_B.set_ylabel('Probability density')
        ax_B.set_title(r'{} with $\mu={:.4f}$, $\sigma={:.4f}$'.format(_title, mean_B, std_B))

        # Tweak spacing to prevent clipping of ylabel
        fig.tight_layout()
        # plt.show()
        fig.savefig(os.path.join(hist_path, "%d_histogram.png" % (_index)), dpi=300)

    @staticmethod
    def plot_weights_histogram(_weights, _ax, _bin, _canvas):
        mean = np.mean(_weights)
        std = np.std(_weights)
        # clear the previouse axis
        _ax.clear()
        n, bins, patches = _ax.hist(
            _weights, _bin, density=1, facecolor="green", alpha=0.75
        )
        y = ((1 / (np.sqrt(2 * np.pi) * std)) * np.exp(-0.5 * (1 / std * (bins - mean)) ** 2))
        _ax.plot(bins, y, '--')
        # self._ax.set_xlabel("Smarts")
        # self._ax.set_ylabel("Probability")
        _ax.set_title(r'$\mu={:.4f}$, $\sigma={:.4f}$'.format(mean, std))
        _ax.grid(True)
        _canvas.draw()

    @staticmethod
    def plot_kernel_criticality(ax, fig, worst_x, worst_x_means, worst_x_stds, colors, error_kw, fig_dir, layer, text,
                                _fontsize, _edge_color, _headline, _num_of_kernels):
        text_kwargs = dict(ha='center', va='center', fontsize=10, color='black', in_layout=True)
        ax.clear()
        y_pos = np.arange(len(worst_x))
        ax.barh(y_pos, worst_x_means, xerr=worst_x_stds, alpha=0.5, align='center', color=colors, error_kw=error_kw)
        ax.invert_yaxis()  # labels read top-to-bottom
        ax.set_yticks(y_pos)
        ax.set_yticklabels(worst_x)
        ax.set_xlabel('Criticality', fontsize=_fontsize)
        ax.set_ylabel('Indices', fontsize=_fontsize)
        ax.set_title("{}critical neurons for layer: {}".format(_headline, layer), fontsize=_fontsize + 2)
        # ax.set_title("{}".format(layer), fontsize=8)
        if _edge_color == "blue":
            x_max = np.max(worst_x_stds)
            y_max = 0.1
            plt.text(x_max - 0.125, y_max, 'Number of kernels:\n {}'.format(_num_of_kernels),
                     bbox=dict(facecolor='blue', alpha=0.5), **text_kwargs)
        elif _edge_color == "red":
            x_max = np.max(worst_x_stds)
            x_max = 2.125
            y_max = len(worst_x) - 1.125
            plt.text(x_max, y_max, 'Number of kernels:\n {}'.format(_num_of_kernels),
                     bbox=dict(facecolor='red', alpha=0.5), **text_kwargs)

        # plt.margins(0.2)
        # Tweak spacing to prevent clipping of tick-labels
        fig.set(edgecolor=_edge_color, linewidth=4, alpha=0.5)
        fig.tight_layout()
        # ax.subplots_adjust(bottom=0.25)
        # fig.savefig(os.path.join(fig_dir, _models_name + "_" + layer + "_criticality_result.pdf"), bbox_inches='tight')
        fig.savefig(os.path.join(fig_dir, text), bbox_inches='tight')

    @staticmethod
    def define_color(x_means, cri_tau):
        colors = list()
        for index in range(len(x_means)):
            criticality = x_means[index]  # + top_x_stds[index]

            if criticality > cri_tau:
                colors.append('red')
            elif criticality > cri_tau / 2:
                colors.append('orange')
            elif criticality > cri_tau / 10:
                colors.append('yellow')
            elif criticality < 0.0:
                colors.append('blue')
            else:
                colors.append('green')
        return colors

    def filter_neurons(critical_neurons, anti_or_critical):
        def nested_dict():
            return collections.defaultdict(nested_dict)

        temp_unsorted_dict = nested_dict()

        for classes, layers in critical_neurons.items():
            for layer, kernels in layers.items():
                for kernel, criticality in kernels.items():
                    cri = criticality[0] + (criticality[1] / 2.0)
                    if anti_or_critical == "anti":
                        # [0]mean + [1]std
                        if cri < 0.0:
                            temp_unsorted_dict[cri][classes]["layer"] = layer
                            temp_unsorted_dict[cri][classes]["kernel"] = kernel
                    elif anti_or_critical == "most":
                        # [0]mean + [1]std
                        if cri > 0.0:
                            temp_unsorted_dict[cri][classes]["layer"] = layer
                            temp_unsorted_dict[cri][classes]["kernel"] = kernel

        if anti_or_critical == "anti":
            return dict(sorted(temp_unsorted_dict.items()))
        else:
            return dict(sorted(temp_unsorted_dict.items(), reverse=True))


    @staticmethod
    def plot_overlapping_matrix(data, labels_x, labels_y, layer, path, settings):
        plt.rcParams.update({'font.size': settings["font_size"]})
        fig = plt.figure(figsize=settings["fig_size"])
        ax = plt.gca()

        im = ax.matshow(data, interpolation='none')
        fig.colorbar(im)
        if settings["show_numbers"]:
            for (i, j), z in np.ndenumerate(data):
                ax.text(j, i, '{:2.0f}'.format(z), ha='center', va='center', size=settings["font_size"])

        ax.set_xticks(np.arange(len(labels_x)))
        ax.set_xticklabels(labels_x)
        ax.set_yticks(np.arange(len(labels_y)))
        ax.set_yticklabels(labels_y)

        # Set ticks on both sides of axes on
        ax.tick_params(axis="x", bottom=False, top=True, labelbottom=False, labeltop=True,
                       labelsize=settings["font_size"])
        ax.tick_params(axis="y", labelsize=settings["font_size"])
        # Rotate and align bottom ticklabels
        # plt.setp([tick.label1 for tick in ax.xaxis.get_major_ticks()], rotation=45,
        #         ha="right", va="center", rotation_mode="anchor")
        ## Rotate and align top ticklabels
        plt.setp([tick.label2 for tick in ax.xaxis.get_major_ticks()], rotation=45,
                 ha="left", va="center", rotation_mode="anchor")

        # ax.set_title("Correlation between anti-critical neurons", pad=10)
        # fig.tight_layout()
        plt.savefig(os.path.join(path, layer + "_correlation.png"), bbox_inches='tight', dpi=600)

    @staticmethod
    def find_overlapping_critical_neurons(_stat_dict, _path):

        settings = {"fig_size": (12, 10),
                    "font_size": 14,
                    "show_numbers": True}

        def nested_dict():
            return collections.defaultdict(nested_dict)

        between_label_correlation = nested_dict()

        # find overlapping neurons for all classes
        for layer, labels in _stat_dict.items():
            copy_labels_dict = copy.deepcopy(labels)
            for i, (label, critical_indices) in enumerate(labels.items()):
                if label in analyzed_labels:
                    ''' For all classes'''
                    # this is controversial since I'm removing layers without anti-critical neurons
                    list_of_indices = list(critical_indices.keys())

                    ''' Correlation between classes '''
                    for j, (copy_label, copy_of_critical_indices) in enumerate(copy_labels_dict.items()):
                        if copy_label in analyzed_labels:
                            copy_of_indices = list(copy_of_critical_indices.keys())
                            between_label_correlation[layer][label][copy_label] = set(copy_of_indices) & set(
                                list_of_indices)

        # filter all empty labels
        between_label_overlapping_filtered = nested_dict()
        for layer, overlapping_matrices in between_label_correlation.items():
            for label_is, entry_is in overlapping_matrices.items():
                not_empty = list()
                for label_js, entry_js in entry_is.items():
                    if len(entry_js) > 0:
                        not_empty.append(entry_js)

                if len(not_empty) > 0:
                    between_label_overlapping_filtered[layer] = overlapping_matrices

        if not os.path.exists(_path):
            os.makedirs(_path)

        for layer, overlapping_matrices in between_label_overlapping_filtered.items():
            all_labels = list(overlapping_matrices.keys())
            matrix_size = len(all_labels)
            overlapping_matrix = np.zeros(shape=(matrix_size, matrix_size), dtype=int)
            for i, (label_i, entry_i) in enumerate(overlapping_matrices.items()):
                for j, (label_j, entry_j) in enumerate(entry_i.items()):
                    overlapping_matrix[i][j] = len(entry_j)
            Visualization.plot_overlapping_matrix(overlapping_matrix, all_labels, all_labels, layer, _path, settings)

        return between_label_correlation

    @staticmethod
    def plot_histogram_with_err(fig_dir, _model, _class, _n_worst, list_of_worst_neurons, list_of_accuracy,
                                list_of_accuracy_stds):
        # plt.rcParams.update({'font.size': 6})
        error_kw = {'elinewidth': 0.5, 'capsize': 2, 'capthick': 0.5, 'ecolor': 'black'}
        fig, ax = plt.subplots(figsize=(7, 3))

        bars_list = ax.bar(list_of_worst_neurons, list_of_accuracy, yerr=list_of_accuracy_stds, error_kw=error_kw,
                           alpha=0.5)
        if _n_worst < 21:
            plt.rcParams.update({'font.size': 10})
            labels = ax.get_xticklabels()
            plt.setp(labels, rotation=45, horizontalalignment='right', fontsize=8)
        else:
            plt.rcParams.update({'font.size': 8})
            ax.xaxis.set_ticklabels([])  # hiding all labels
        bars_list[0].set_color("g")  # setting first bar as green - no masking
        # ax.set_title("Accuracy of most {} critical neurons for model {} and for class {}".format(_n_worst, _model, _class), fontsize=8)
        ax.set_ylabel('Accuracy')
        # ax.set_xlabel('Layers\' names')
        fig.tight_layout()
        fig.savefig(os.path.join(fig_dir, _model + "_" + _class + "_" + str(_n_worst) + "_accuracy_result.pdf"))

    @staticmethod
    def plot_CDP_dependently_masking_results(_path, _statistics_dict, _or_conf, _class, _image_name, _model_name,
                                             _number_of_neurons):

        # statistics_dict[every_layer].append({max_response_filter_index: or_conf})
        layers_names = list()
        texts = list()
        values = list()

        for index, each_layer in enumerate(tqdm(_statistics_dict.keys())):
            layers_names.append(each_layer)
            texts.append(list(_statistics_dict[each_layer][0].keys())[0])
            values.append(list(_statistics_dict[each_layer][0].values())[0])

        # differences = np.diff(np.asarray(values))
        differences = np.gradient(np.asarray(values), 2)
        np.insert(differences, 0, abs(_or_conf - values[0]) / 2.0)
        # print(differences)
        # print(differences.shape)

        plt.rcParams.update({'font.size': 10})
        fig, ax = plt.subplots()

        ax.bar(layers_names, np.asarray(values), alpha=0.5)

        for index, i in enumerate(ax.patches):
            # get_x pulls left or right; get_height pushes up or down
            ax.text(i.get_x() + .12, i.get_height() - 3, \
                    str(format(values[index], '.3f')), fontsize=10,
                    color='white')

        # plt.tick_params(labelright=True)
        # ax.set_xticks(x_pos, layers)#'vertical')
        labels = ax.get_xticklabels()
        plt.setp(labels, rotation=45, horizontalalignment='right')
        # ax.set_title("Layers\' dependently masking confidence for class: {}".format(_class))
        ax.set_ylabel('Confidence gradient')
        ax.set_xlabel('Layers\' names')
        # plt.margins(0.2)
        # Tweak spacing to prevent clipping of tick-labels
        # fig.tight_layout()
        # ax.subplots_adjust(bottom=0.25)
        fig_dir = os.path.join(_path, _model_name, _image_name, "criticality")
        if not os.path.exists(fig_dir):
            os.makedirs(fig_dir)
        fig.savefig(os.path.join(
            fig_dir + "Layers_dependently_masking_confidence_result_" + _class + str(_number_of_neurons) + ".png"))

    @staticmethod
    def plot_layers_responses_results(_path, _image_name, _model_name, _class, _sum_responses_dict,
                                      _parameter_dict=None):
        fig, ax = plt.subplots()

        for layer in _sum_responses_dict.keys():
            fm_sum_list = _sum_responses_dict[layer][0]
            fm_sum_np = np.asarray(fm_sum_list)
            norm_fm_sum_np = fm_sum_np / np.max(fm_sum_np)

            ''' --------------------------------------- '''
            ''' Plotting histogram of fm sums '''
            ''' --------------------------------------- '''
            # clear the previouse axis
            ax.clear()

            x_pos = np.arange(len(fm_sum_list))
            ax.barh(x_pos, norm_fm_sum_np, alpha=0.5, align='center')
            ax.invert_yaxis()  # labels read top-to-bottom
            ax.set_xlabel('Neuron\'s response')
            ax.set_ylabel('Weights\' indices')
            # ax.set_title("Neurons\' reponses for class {}".format(_class))

            fig_dir = os.path.join(_path, _model_name, _class, _image_name, "responses_graph")
            if not os.path.exists(fig_dir):
                os.makedirs(fig_dir)
            fig.savefig(os.path.join(fig_dir, layer + "_graph.png"))

    @staticmethod
    def get_labels(legend_labels, number_of_labels=32):
        # produce a legend with the unique colors from the scatter
        legend_label = list()
        if legend_labels.shape[0] > number_of_labels:
            indices = np.linspace(0, legend_labels.shape[0] - 1, number_of_labels).astype(int)
            for i in indices:
                name = copy.deepcopy(legend_labels[i])
                for remove_str in remove_string:
                    name = name.replace(remove_str, "")
                legend_label.append(name)
        else:
            legend_label = list(legend_labels)
            number_of_labels = len(legend_label)

        return legend_label, number_of_labels

