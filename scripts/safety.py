import collections
from collections import defaultdict
import copy
import json
import logging
import os
import numpy as np
import torch
from tqdm import tqdm


def get_layers_dimension(_weights) -> list():
    if len(_weights.shape) == 4:
        # point-wise
        if _weights.shape[2] == 1 and _weights.shape[3] == 1:
            return [index for index in range(_weights.shape[0])]
        elif _weights.shape[1] == 1:
            return [index for index in range(_weights.shape[0])]
        # depth-wise
        else:
            return [index for index in range(_weights.shape[0])]

    elif len(_weights.shape) == 1:
        return [index for index in range(_weights.shape[0])]

    # to filter only dense layers
    elif len(_weights.shape) == 2:
        return [index for index in range(_weights.shape[0])]

    else:
        return [None]


def unmask_kernel(_weights, _original_weights, _index: int):
    if len(_weights.shape) == 4:  # to filter only depth separable and conv layers
        _weights[_index, :, :, :] = copy.deepcopy(_original_weights[_index, :, :, :])

    # to filter only dense layers
    elif len(_weights.shape) == 2:
        _weights[_index, :] = copy.deepcopy(_original_weights[_index, :])

    elif len(_weights.shape) == 1:
        _weights[_index] = copy.deepcopy(_original_weights[_index])


def multiple_mask_kernel(_weights, _indices: list):
    if len(_weights.shape) == 4:  # to filter only depth separable and conv layers
        for filters in _indices:
            _weights[filters, :, :, :] = 0.0

    # to filter only dense layers
    elif len(_weights.shape) == 2:
        for neurons in _indices:
            _weights[neurons, :] = 0.0

    elif len(_weights.shape) == 1:
        for neurons in _indices:
            _weights[neurons] = 0.0


def mask_kernel(_weights, _index: int):
    # to filter only depth separable and conv layers
    if len(_weights.shape) == 4:
        _weights[_index, :, :, :] = 0.0

    # to filter only dense layers
    elif len(_weights.shape) == 2:
        _weights[_index, :] = 0.0

    elif len(_weights.shape) == 1:
        _weights[_index] = 0.0


def kernels_ln_norm(_weights, _index: int, _l_norm=1):
    # to filter only depth separable and conv layers
    if len(_weights.shape) == 4:
        return torch.norm(_weights[_index, :, :, :], p=_l_norm)

    # to filter only dense layers
    elif len(_weights.shape) == 2:
        return torch.norm(_weights[_index, :], p=_l_norm)

    elif len(_weights.shape) == 1:
        return torch.norm(_weights[_index], p=_l_norm)

    else:
        return 0.0


def get_conf_and_class(self, des_cls, input_batch, remove=False):

    # Get outputs from chosen layers and calculate maximum responses
    output = self.MappedModel(input_batch)
    probabilities = torch.nn.functional.softmax(output, dim=1)
    probabilities = probabilities.cpu().detach().numpy()

    pre_ind = np.argmax(probabilities, axis=1)
    pre_conf = np.max(probabilities, axis=1)
    pre_cls = [self.DataSet.list_of_classes_labels[index] for index in pre_ind]

    des_ind = [self.DataSet.list_of_classes_labels.index(index) for index in des_cls]
    des_conf = [probabilities[index][label_index] for index, label_index in enumerate(des_ind)]

    return pre_ind, pre_conf, des_ind, des_conf
